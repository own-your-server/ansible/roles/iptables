# IPtables

Permet d'ajouter des règles Iptables au serveur. Ce script est compatible avec docker.

Par défaut toutes les IO vont être bloquées. Par la suite on va ouvrir les ports en fonction des variables.

Par défaut les protocoles suivants sont ouvert: 
* loopback
* icmp => ping ouvert
* 53 => DNS ouvert
* 123 => NTP ouvert

Les IPv46 sont bloqués par défaut.

## Installation

### Variables
* **input_ports** (default:[22]) => liste des port ouvert en entré. Attention en cas de changement il faut penser à remettre le port 22 (si vous en avez besoin)
* **output_ports**: [] => liste des port de sortie.

## évolution

Le script est dans le dossier template.
On commence par couper docker puis on le remet pour qu'il puisse recréer ses règles iptables custom.
